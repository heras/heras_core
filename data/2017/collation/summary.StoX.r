#Set up directories

#directories to extract  data from 
StoXDir<- "C:/Users/Lusseaus/workspace/stox/project/" #Directory path where StoX workspace sits

ProjectName<-   "HERAS_2017_SPR"
ProjectName<-   "HERAS_2017_HER"
ProjectName<-   "MSHAS_2017_HER"
ProjectName<-   "MSHAS_2018_HER_MO"
ProjectName<-   "MSHAS_2018_HER"
ProjectName<-   "6aSPAWN_2019_Pathway_HER"


# directory for results
pathOut<-"C:/Users/Lusseaus/Documents/Acoustics/NorthSea Acoustic surveys/1 HERAS COORDINATION/HERAS Analysis"

# Create WD name and set working directory to the project
StoXbaseline<-paste(StoXDir,ProjectName,"/output/baseline/report",sep="")

setwd(StoXbaseline)

#read in data from StoX outputs
num <- read.delim("1_FillMissingData_SuperIndividuals.txt",stringsAsFactors = FALSE)

#calculate biomass

#missing weight?
summary(num$weight)
idx <- which(num$weight=="-") #blank weigths
#if so proceed with care - and either remove or change to NA before proceeding:
num$weight[idx]<-"NA"

num$biomass<-as.numeric(as.character(num$Abundance))*as.numeric(as.character(num$weight))
idx<-which(is.na(num$biomass))
num[idx,] #all NA from where weight is NA. 

#If no Stock aggregation needed use this:
#aggregate at chosen level.
abun<-aggregate(num$Abundance, list(num$Stratum, num$specialstage), sum)
names(abun)<-c("strata", "maturity","number")

#aggregate with length group
abun2<-aggregate(num$Abundance, list(num$Stratum, num$length ,num$age, num$specialstage), sum)
names(abun2)<-c("strata","length","age","maturity","number")

#same for biomass
biom<-aggregate(num$biomass, list(num$Stratum, num$specialstage), sum)
names(biom)<-c("strata","maturity","biomass_g")

#with length
biom2<-aggregate(num$biomass, list(num$Stratum, num$length ,num$age, num$specialstage), sum)

names(biom2)<-c("strata","length","age","maturity","biomass_g")


#---------------------------------------------------------------------------
#If aggregating by stock use this instead:

#aggregate at chosen level.
abun<-aggregate(num$Abundance, list(num$Stratum, num$specialstage, num$stage), sum)
names(abun)<-c("strata", "maturity","stock","number")

#aggregate with length group
abun2<-aggregate(num$Abundance, list(num$Stratum, num$length ,num$age, num$specialstage, num$stage), sum)
names(abun2)<-c("strata","length","age","maturity","stock","number")

#same for biomass
biom<-aggregate(num$biomass, list(num$Stratum, num$specialstage, num$stage), sum)
names(biom)<-c("strata","maturity","stock","biomass_g")

#with length
biom2<-aggregate(num$biomass, list(num$Stratum, num$length ,num$age, num$specialstage, num$stage), sum)

names(biom2)<-c("strata","length","age","maturity","stock","biomass_g")


#--------------------------------------------------------------------------------

#combine into one
all<-abun
all$biomass_g<- biom$biomass_g
all$meanW_g<-all$biomass/all$number

all2<-abun2
all2$biomass_g<- biom2$biomass_g
all2$meanW_g<-all2$biomass/all2$number


# Output to csv file
write.csv(all,paste(pathOut,"/",ProjectName,".csv",sep=""),row.names = FALSE)
write.csv(all2,paste(pathOut,"/",ProjectName,"2",".csv",sep=""),row.names = FALSE)

write.csv(all,"RoutPathway2019.csv",row.names = FALSE)


#-------------------------------------------------------------------------------------

# Strata covered by DK has quite a few stock and maturity assignemnts missing due to bio sampling effort (missing individual stock info for some length/maturity/age combination)
# DK covered strata 21,31,41,42,151, 152 in 2017. the data for these strata is compiled outside of R using DK individual stock/maturity/age/length keys to assign most likely stock/maturity to those portions of abundance missing these.
# For strata covered by SCO, NED, GER only one len is missing an age - one fish of lengt 36.5cm Age assume to be 11 whcih is the oldest assigned age in the survey otherwise.

#Summaries for NSAS

# remove strata covered by DK from calcs, these have been handled outside R

strata.out<-c(152, 21, 151,  41,  42,  31)
index<-which(all2$strata %in% strata.out)
all2[index,]

a<-all2[-index,]

head(a)
a$lensum<-a$length*a$number

#Summaries for each strata by stock for tables 5.14 and 5.15
strat.num <- aggregate(a$number,list(a$maturity,a$strata),sum)
names(strat.num)<-c("mat","strata","num")
strat.num$num_mill<-strat.num$num/1000000
strat.biom <-aggregate(a$biomass_g,list(a$maturity,a$strata),sum)
names(strat.biom)<-c("mat","strata","biom_g")
strat.biom$biom_kt <-strat.biom$biom_g/1000000000

#write out
write.csv(strat.biom,paste(pathOut,"/","strata biomass.csv",sep=""),row.names = FALSE)
write.csv(strat.num,paste(pathOut,"/","strata numbers.csv", sep=""),row.names = FALSE)

#Summaries by age and maturity
sums <- aggregate(a$number,list(a$maturity,a$age),sum)
bioms <- aggregate(a$biomass_g,list(a$maturity,a$age),sum)
lsums<-aggregate(a$lensum,list(a$maturity,a$age),sum)

names(sums) <-c("mat","age","num")
names(bioms) <-c("mat","age","biom_g")
names(lsums) <-c("mat","age","lensum")

all.sums<-sums
all.sums$biom_t <- bioms$biom_g/1000000
all.sums$num_thous <- all.sums$num/1000
all.sums$mean_len <- lsums$lensum/sums$num
all.sums$mean_wt <- bioms$biom_g/sums$num


#write out
write.csv(all.sums,paste(pathOut,"/","NSAS collated.csv",sep=""),row.names = FALSE)
