#Set up directories

#directories to extract  data from 
StoXDir<- "C:/Users/smalu/workspace/stox/project/" #Directory path where StoX workspace sits

ProjectName<-   "2020_HERAS_HER_EU" #name of StoX project

# directory for results
pathOut<-"J:/git/heras_index_kbwot/data/2020/collation"

# Create WD name and set working directory to the project
StoXbaseline <- paste(StoXDir,ProjectName,"/output/baseline/report",sep="")

#setwd(StoXbaseline)
setwd(pathOut)

#read in data from StoX outputs
num <- read.delim("1_FillMissingData_SuperIndividuals.txt",stringsAsFactors = FALSE)

#calculate biomass

#missing weight?
summary(num$weight)
idx <- which(num$weight=="-") #blank weigths
#if so proceed with care - and either remove or change to NA before proceeding:
num$weight[idx]
#num$weight[idx]<-"NA"

num$biomass<-as.numeric(as.character(num$Abundance))*as.numeric(as.character(num$weight))
idx<-which(is.na(num$biomass))
num[idx,] #all NA from where weight is NA. 

#---------------------------------------------------------------------------
#If aggregating by stock use this:

#aggregate at chosen level.
#abun<-aggregate(num$Abundance, list(num$Stratum, num$specialstage, num$stage), sum)
#names(abun)<-c("strata", "maturity","stock","number")

#aggregate with length group
abun2<-aggregate(num$Abundance, list(num$Stratum, num$length ,num$age, num$specialstage, num$stage), sum)
names(abun2)<-c("strata","length","age","maturity","stock","number")

#same for biomass
#biom<-aggregate(num$biomass, list(num$Stratum, num$specialstage, num$stage), sum)
#names(biom)<-c("strata","maturity","stock","biomass_g")

#with length
biom2<-aggregate(num$biomass, list(num$Stratum, num$length ,num$age, num$specialstage, num$stage), sum)
names(biom2)<-c("strata","length","age","maturity","stock","biomass_g")


#--------------------------------------------------------------------------------

#
all2<-abun2
all2$biomass_g <- biom2$biomass_g
all2$meanW_g <- all2$biomass/all2$number


# Output to csv file for filling in missing stock and maturity in .xls
#write.csv(all,paste(pathOut,"/",ProjectName,".csv",sep=""),row.names = FALSE)
write.csv(all2,paste(pathOut,"/",ProjectName,"2",".csv",sep=""),row.names = FALSE)


#--- END ----------------------------------------------------------------------------------

