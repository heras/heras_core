clear all
close all
clc

path = pwd;

bioTab = readtable(fullfile(path,'Biotic_26D4201806_bio.csv'));

uniqueHauls = unique(bioTab.HaulNumber);

for idxHauls = 1:length(uniqueHauls)
    idxFilt = find(bioTab.HaulNumber == uniqueHauls(idxHauls));
    bioTabFilt = bioTab(idxFilt,:);
    uniqueSpecies = unique(bioTabFilt.CatchSpeciesCode);
    for idxSpecies = 1:length(uniqueSpecies)
        idxFilt2 = find(bioTabFilt.CatchSpeciesCode == uniqueSpecies(idxSpecies));
        bioTabFilt2 = bioTabFilt(idxFilt2,:);
%         bioTabFilt2.BiologyFishID = 1:size(bioTabFilt2,1);
        bioTab.BiologyFishID(idxFilt(idxFilt2)) = 1:size(bioTabFilt2,1);
    end
end

writetable(bioTab,fullfile(path,'Biotic_26D4201806_bio_correct.csv'))