rm(list=ls())

#Set up directories

#directories to extract  data from 
StoXDir<- "J:/git/heras_index_kbwot/StoX/2018/" #Directory path where StoX workspace sits\
#StoXDir<- "J:/git/heras_index_kbwot/temp/" #Directory path where StoX workspace sits

#StoXDir<- "C:/Users/Lusseaus/workspace/stox/project/" #Directory path where StoX workspace sits

#ProjectName <-  "01 Post-cruise - HERAS_2018_HER"
#ProjectName <-  "02 HERAS_2018_HER"
ProjectName <-  "2018_HERAS_HER_EU"

# directory for results
pathOut<-"J:/git/heras_index_kbwot/attic/collation/2018"

# Create WD name and set working directory to the project
StoXbaseline<-paste(StoXDir,ProjectName,"/output/baseline/report",sep="")

#StoXbaseline<-"J:/git/heras_index_kbwot/attic/collation/2018"

setwd(StoXbaseline)

#read in data from StoX outputs
num <- read.delim("1_FillMissingData_SuperIndividuals.txt",stringsAsFactors = FALSE)

#calculate biomass

#missing weight?
summary(num$weight)
idx <- which(num$weight=="-") #blank weigths
#if so proceed with care - and either remove or change to NA before proceeding:
num$weight[idx]<-"NA"

num$biomass<-as.numeric(as.character(num$Abundance))*as.numeric(as.character(num$weight))
idx<-which(is.na(num$biomass))
num[idx,] #all NA from where weight is NA. 

#Aggregating by stock, length, age and maturity:

#aggregate at chosen level.
abun<-aggregate(num$Abundance, list(num$Stratum, num$length ,num$age, num$specialstage, num$stage), sum)
names(abun)<-c("strata","length","age","maturity","stock","number")

#same for biomass
biom<-aggregate(num$biomass, list(num$Stratum, num$length ,num$age, num$specialstage, num$stage), sum)
names(biom)<-c("strata","length","age","maturity","stock","biomass_g")


#--------------------------------------------------------------------------------

#combine into one
all<-abun
all$biomass_g<- biom$biomass_g
all$meanW_g<-all$biomass/all$number


# Output to csv file
write.csv(all,paste(pathOut,"/",ProjectName,".csv",sep=""),row.names = FALSE)

################--  END  --##################################################

