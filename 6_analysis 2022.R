rm(list=ls())

library(FLCore)
library(ggplot2)
library(tidyverse)
library(viridis)

path <- dirname(rstudioapi::getActiveDocumentContext()$path)

try(setwd(path),silent=TRUE)

mainPath      <- file.path(".")
dataPath      <- file.path(".","data")
stoxPath      <- file.path(".","StoX")
functionPath  <- file.path(".","functions")
resultsPath   <- file.path(".","results")
figurePath    <- file.path(".","figures",'report',"2022","analysis")

load(file.path(resultsPath,'HERAS_baseline_SPR.RData'))

#HERAS_baseline.SPR$HERAS.SPR$HERAS_strata <- HERAS_baseline.SPR$HERAS.SPR$HERAS_strata[,ac(2017:2021)]
#HERAS_baseline.SPR$HERAS.SPR$HERAS_all    <- HERAS_baseline.SPR$HERAS.SPR$HERAS_all[,ac(2017:2021)]
#HERAS_baseline.SPR$stk.SPR                <- HERAS_baseline.SPR$stk.SPR[,ac(2017:2021)]

###############################################################################
## make tables

idxStrata <- as.data.frame(yearSums(quantSums(HERAS_baseline.SPR$HERAS.SPR$HERAS_strata@index[,,'biomass','all'])))
idxStrata <- idxStrata[idxStrata$data != 0,]
idxStrata <- idxStrata$area

SPR.strata  <- as.data.frame(HERAS_baseline.SPR$HERAS.SPR$HERAS_strata@index[,,,,ac(idxStrata)])
SPR.all     <- as.data.frame(HERAS_baseline.SPR$HERAS.SPR$HERAS_all@index)

prop.131 <- HERAS_baseline.SPR$HERAS.SPR$HERAS_strata@index[,,'biomass','all',ac(131)]/HERAS_baseline.SPR$HERAS.SPR$HERAS_all@index[,,'biomass','all']
prop.131 <- as.data.frame(prop.131)

###############################################################################
## plotting

scaling_factor <- 2
png(file.path(figurePath,'SPR_abundance age composition per strata.png'), 
    width = 12*scaling_factor, height = 8*scaling_factor, units = "cm", res = 300, pointsize = 10)

p <- ggplot(subset(SPR.strata,season=='all' & unit=='abu'),aes(x=year,y=data))+
      theme_bw()+
      geom_bar(aes(fill = as.factor(age)),stat="identity")+
      ylab('abundance')+
      #scale_fill_viridis()+
      theme(axis.text.x = element_text(angle=90))+
      facet_wrap(~area)#,scale='free_y'

print(p)
dev.off()

scaling_factor <- 2
png(file.path(figurePath,'SPR_biomass age composition per strata.png'), 
    width = 12*scaling_factor, height = 8*scaling_factor, units = "cm", res = 300, pointsize = 10)

p <- ggplot(subset(SPR.strata,season=='all' & unit=='biomass'),aes(x=year,y=data,col=as.factor(age)))+
  theme_bw()+
  geom_bar(aes(fill = as.factor(age)),stat="identity",position="stack")+
  #geom_line()+
  ylab('biomass')+
  #scale_fill_viridis()+
  theme(axis.text.x = element_text(angle=90))+
  facet_wrap(~area,scale='free')

print(p)
dev.off()

scaling_factor <- 2
png(file.path(figurePath,'SPR_biomass age proportion 131.png'), 
    width = 12*scaling_factor, height = 8*scaling_factor, units = "cm", res = 300, pointsize = 10)

p <- ggplot(subset(prop.131,year<=2021),aes(x=year,y=data,col=as.factor(age)))+
  theme_bw()+
  geom_line()+
  ylab('Proportions relative to total biomass at age')+
  #scale_fill_viridis()+
  theme(axis.text.x = element_text(angle=90))+
  facet_wrap(~area,scale='free')

print(p)
dev.off()
