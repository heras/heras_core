rm(list=ls())

library(FLCore)
library(ggplot2)
library(tidyverse)
library(maps)
library(mapdata)
library(rgdal)
library(lubridate)
library(broom)

path <- 'C:/git/heras_index'

try(setwd(path),silent=TRUE)

mainPath      <- file.path(".")
dataPath      <- file.path(".","data")
stoxPath      <- file.path(".","StoX")
functionPath  <- file.path(".","functions")
resultsPath   <- file.path(".","results")
figurePath    <- file.path(".","figures",'raw_data')

Area <- map_data("worldHires", region =c("Norway", "Germany", "Denmark", "Netherlands", "Belgium", "Ireland", "Sweden", "UK"))

HERAS_Strata    <- readOGR(file.path(dataPath,'strata','shp','HERAS2017_StoX_polygons.shp')) #set to wd where you stored your shp-files
MSHAS_Strata    <- readOGR(file.path(dataPath,'strata','shp','MSHAS2017_polygon.shp')) #set to wd where you stored your shp-files
#convert shapefile into dataframe to be used by ggplot2
HERAS_Strata_df <- tidy(HERAS_Strata)
MSHAS_Strata_df <- tidy(MSHAS_Strata)

years <- c(2018,2019,2020,2021,2022)

for(idxYear in years){
  # plotting acoustics
  load(file.path(dataPath,idxYear,'raw_data',paste0(idxYear,'_HERAS.Rdata')))
  
  DataAll <- merge(DataAll,CruiseAll,by = 'CruiseLocalID')
  DataAll$year <- year(DataAll$LogTime)
  DataAll$LogLatitude <- as.numeric(DataAll$LogLatitude)
  DataAll$LogLongitude <- as.numeric(DataAll$LogLongitude)
  DataAll$DataValue <- as.numeric(DataAll$DataValue)
  DataAll$LogTime <- as.POSIXct(DataAll$LogTime,format='%Y-%m-%dT%H:%M:%S',tz='CET')
  DataAll$LogDistance <- as.numeric(DataAll$LogDistance)
  DataAll$SamplePingAxisInterval <- as.numeric(DataAll$SamplePingAxisInterval)
  
  DataAll <- DataAll %>% 
              group_by(CruiseCountry,
                       LogDistance,
                       LogTime,
                       year,
                       SamplePingAxisInterval,
                       DataSaCategory) %>% 
              summarize(LogLatitude=first(LogLatitude),
                        LogLongitude=first(LogLongitude),
                        SA=sum(DataValue))
  
  DataAll <- DataAll[order(DataAll$LogTime,DataAll$CruiseCountry),]
  
  uniqueCountry <- unique(DataAll$CruiseCountry)
  
  flagFirst <-TRUE
  for(idxCountry in uniqueCountry){
    dataCountry <- subset(DataAll,CruiseCountry==idxCountry)
    dataCountry$breaks <- cut(dataCountry$LogDistance,breaks = seq(from=min(dataCountry$LogDistance),to=max(dataCountry$LogDistance),by=1))
    
    dataCountry <- dataCountry %>%
                    group_by(breaks,CruiseCountry,DataSaCategory) %>%
                    summarize(LogDistance=first(LogDistance),
                              LogTime=first(LogTime),
                              LogLatitude=first(LogLatitude),
                              LogLongitude=first(LogLongitude),
                              SA=mean(SA),
                              LogSum=sum(SamplePingAxisInterval))
    #dataCountry <- dataCountry[dataCountry$LogSum == 5,]
    if(flagFirst){
      dataCountryAll <- dataCountry
      flagFirst <- FALSE
    }else{
      dataCountryAll <- rbind(dataCountryAll,dataCountry)
    }
  }
  
  
  NASC_no0  <- subset(dataCountryAll,DataSaCategory == 'HER' | DataSaCategory == 'CLU' | DataSaCategory == 'MIX') %>% filter(SA>0)
  
  NASC_0    <- dataCountryAll
  
  A <- subset(dataCountryAll,CruiseCountry == 'DE')
  
  #DataAll$DataSaCategory
  png(file.path(figurePath,idxYear,paste0('map_HER.png')), 
      width = 30, height = 30, units = "cm", res = 300, pointsize = 10)
  
  p <- ggplot()+
        geom_polygon(data=Area, aes(long, lat, group=group))+
        geom_point(data=NASC_no0,aes(x=LogLongitude,y=LogLatitude,size = SA,col=DataSaCategory),alpha = 0.5)+
        geom_point(data=NASC_0, aes(x=LogLongitude,y=LogLatitude), size=0.1, shape=3, col="darkgrey")+
        coord_quickmap(xlim=c(-12.0,12.5), ylim=c(51.5,62))+
        #scale_size_area(max_size = 20, breaks = c(500, 1000, 2500, 5000, 10000), name=bquote('NASC'~(m^2~nm^-2)))+
        geom_path(data=HERAS_Strata_df, aes(x = long, y = lat, group = group), colour= "red")+
        geom_path(data=MSHAS_Strata_df, aes(x = long, y = lat, group = group), colour= "red")+
        coord_quickmap(xlim=c(-12.0,12.5), ylim=c(51.5,62))+
        scale_x_continuous(breaks=seq(-12,12,2))+
        scale_y_continuous(breaks=seq(51,62,1), sec.axis=dup_axis(name=NULL, labels=NULL))+
        labs(
          #title = "HERAS Herring mean NASC per 5 nmi EDSU", 
          x = "Longitude ?E", 
          y = "Latitude ?N")+
        theme(legend.justification=c(0,1), legend.position=c(0,1))+
        theme(legend.background = element_rect(size=0.3, linetype="solid",colour ="black"))+
        theme (axis.title = element_text(size=16, face="bold"),
               axis.text = element_text(size=14, color="black", face="bold"),
               legend.title = element_text(size=14, face="bold"),
               legend.text = element_text(size=12, face="bold"))
  
  print(p)
  dev.off()
}