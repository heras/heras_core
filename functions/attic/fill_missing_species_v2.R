fill_missing_species_v2 <- function(endTab){
  
  endTab <- endTab2
  
  endTab<-aggregate(endTab$Abundance, 
                    list( endTab$Stratum, 
                          endTab$length, # 
                          endTab$age, 
                          endTab$specialstage, 
                          endTab$stage), 
                  sum)
  
  endTab$filling <- 0
  names(endTab)<-c("strata","length","age","maturity","stock","number","filling")
  
  
  ###########################################
  # find missing cells for obvious case
  ###########################################
  
  idxFillMat <- which(  endTab$maturity == '-'| 
                          endTab$stock == '-' | 
                          endTab$age == '-' | 
                          endTab$length == '-')
  
  endTabOut <- endTab[idxFillMat,]
  
  # fill in age 0 to immature
  idxEmptyMat     <-  which(endTabOut$maturity=="-")
  idxTemp <- idxEmptyMat[endTabOut$age[idxEmptyMat] == 0]
  endTabOut[idxTemp,]$maturity <- 'IMM'
  
  # fill in fish of length < 8.5 cm as IMM
  idxEmptyMat   <-  which(endTabOut$maturity=="-")
  idxTemp       <- idxEmptyMat[endTabOut$length[idxEmptyMat] < 8.5]
  endTabOut[endTabOut$length < 8.5,]$maturity <- 'IMM'
  
  # replace values in tab
  endTab[idxFillMat,] <- endTabOut
  
  ################################################
  # find missing cells for more difficult cases
  ################################################
  
  idxFillMat <- which(  endTab$maturity == '-'| 
                          endTab$stock == '-' | 
                          endTab$age == '-' | 
                          endTab$length == '-')
  
  endTabOut <- endTab[idxFillMat,]
  
  for(idxSpecimen in 1:dim(endTabOut)[1]){
    missingFields <- c(endTabOut[idxSpecimen,]$stock == '-', endTabOut[idxSpecimen,]$maturity == '-')
    
    
    # if both fields missing
    if(all(missingFields == c(TRUE, TRUE))){
      
      # 1: check strata. NSAS if part of a strata other than those where mixing is expected
      if(is.na(any(match(c(151,152,41,42,31,21),endTabOut$strata[idxSpecimen])))){
        endTabOut[idxSpecimen,]$stock <- 'her-47d3'
      }else{# 2: if in strata with NSAS/WBSS mixing, assign to highest probability based on maturity and age. If not age check -+2 cm of length
        
        endTabFiltAge <- endTab[  endTab$strata == endTabOut$strata[idxSpecimen] &
                                    endTab$age == endTabOut$age[idxSpecimen] &
                                    endTab$stock != '-' &
                                    endTab$maturity != '-',]
        endTabFiltLength <- endTab[ endTab$strata == endTabOut$strata[idxSpecimen] &
                                      endTab$length >= endTabOut$length[idxSpecimen]-2 &
                                      endTab$length <= endTabOut$length[idxSpecimen]+2 &
                                      endTab$stock != '-' &
                                      endTab$maturity != '-',]
        endTabFiltAll <- endTab[  endTab$strata == endTabOut$strata[idxSpecimen] &
                                    endTab$stock != '-' &
                                    endTab$maturity != '-',]
        # if there is an age
        if(endTabOut$age[idxSpecimen] != '-' & dim(endTabFiltAge)[1] != 0){
          probAllNSAS <- dim(endTabFiltAge[endTabFiltAge$stock == 'her-47d3',])[1]/dim(endTabFiltAge)[1]
          probAllIMM  <- dim(endTabFiltAge[endTabFiltAge$maturity == 'IMM',])[1]/dim(endTabFiltAge)[1]
        }else if(dim(endTabFiltLength)[1] != 0){
          probAllNSAS <- dim(endTabFiltLength[endTabFiltLength$stock == 'her-47d3',])[1]/dim(endTabFiltLength)[1]
          probAllIMM  <- dim(endTabFiltLength[endTabFiltLength$maturity == 'IMM',])[1]/dim(endTabFiltLength)[1]
        }else{
          probAllNSAS <- dim(endTabFiltAll[endTabFiltAll$stock == 'her-47d3',])[1]/dim(endTabFiltAll)[1]
          probAllIMM  <- dim(endTabFiltAll[endTabFiltAll$maturity == 'IMM',])[1]/dim(endTabFiltAll)[1]
        }
        
        
        if(probAllNSAS >= 0.5)endTabOut[idxSpecimen,]$stock <- 'her-47d3'else endTabOut[idxSpecimen,]$stock <- 'her-3a22'
        if(probAllIMM >= 0.5)endTabOut[idxSpecimen,]$maturity <- 'IMM'else endTabOut[idxSpecimen,]$maturity <- 'MAT'
        
      }
      
      
      # if missing stock ID only
    }else if(which(missingFields) == 1){
      # 1: check strata. NSAS if part of a strata other than those where mixing is expected
      if(is.na(any(match(c(151,152,41,42,31,21),endTabOut$strata[idxSpecimen])))){
        endTabOut[idxSpecimen,]$stock <- 'her-47d3'
        
      }else{# 2: if in strata with NSAS/WBSS mixing, assign to highest probability based on maturity and age. If not age check -+2 cm of length
        endTabFiltAge <- endTab[  endTab$strata == endTabOut$strata[idxSpecimen] &
                                    endTab$maturity == endTabOut$maturity[idxSpecimen] &
                                    endTab$age == endTabOut$age[idxSpecimen] &
                                    endTab$stock != '-',]
        endTabFiltLength <- endTab[ endTab$strata == endTabOut$strata[idxSpecimen] &
                                      endTab$maturity == endTabOut$maturity[idxSpecimen] &
                                      endTab$length >= endTabOut$length[idxSpecimen]-2 &
                                      endTab$length <= endTabOut$length[idxSpecimen]+2 &
                                      endTab$stock != '-',]
        endTabFiltAll <- endTab[  endTab$strata == endTabOut$strata[idxSpecimen] &
                                    endTab$maturity == endTabOut$maturity[idxSpecimen] &
                                    endTab$stock != '-',]
        
        if(endTabOut$age[idxSpecimen] != '-' & dim(endTabFiltAge)[1] != 0){
          probAllNSAS <- dim(endTabFiltAge[endTabFiltAge$stock == 'her-47d3',])[1]/dim(endTabFiltAge)[1]
        }else if(dim(endTabFiltLength)[1] != 0){
          probAllNSAS <- dim(endTabFiltLength[endTabFiltLength$stock == 'her-47d3',])[1]/dim(endTabFiltLength)[1]
        }else{
          probAllNSAS <- dim(endTabFiltAll[endTabFiltAll$stock == 'her-47d3',])[1]/dim(endTabFiltAll)[1]
        }
        # allocate species
        if(probAllNSAS >= 0.5)endTabOut[idxSpecimen,]$stock <- 'her-47d3'else endTabOut[idxSpecimen,]$stock <- 'her-3a22'
      }
      
      # if missing stock maturity only
    }else if(which(missingFields) == 2){
      endTabFiltAge <- endTab[  endTab$strata == endTabOut$strata[idxSpecimen] &
                                  endTab$stock == endTabOut$stock[idxSpecimen] &
                                  endTab$age == endTabOut$age[idxSpecimen] &
                                  endTab$maturity != '-',]
      endTabFiltLength <- endTab[ endTab$strata == endTabOut$strata[idxSpecimen] &
                                    endTab$stock == endTabOut$stock[idxSpecimen] &
                                    endTab$length >= endTabOut$length[idxSpecimen]-2 &
                                    endTab$length <= endTabOut$length[idxSpecimen]+2 &
                                    endTab$maturity != '-',]
      endTabFiltAll <- endTab[  endTab$strata == endTabOut$strata[idxSpecimen] &
                                  endTab$stock == endTabOut$stock[idxSpecimen] &
                                  endTab$maturity != '-',]
      
      # if there is an age
      if(endTabOut$age[idxSpecimen] != '-' & dim(endTabFiltAge)[1] != 0){
        probAllIMM <- dim(endTabFiltAge[endTabFiltAge$maturity == 'IMM',])[1]/dim(endTabFiltAge)[1]
      }else if(dim(endTabFiltLength)[1] != 0){
        probAllIMM <- dim(endTabFiltLength[endTabFiltLength$maturity == 'IMM',])[1]/dim(endTabFiltLength)[1]
      }else{
        probAllIMM <- dim(endTabFiltAll[endTabFiltAll$maturity == 'IMM',])[1]/dim(endTabFiltAll)[1]
      }
      # allocate maturity field
      if(probAllIMM >= 0.5)endTabOut[idxSpecimen,]$maturity <- 'IMM'else endTabOut[idxSpecimen,]$maturity <- 'MAT'
    }
  }
  
  endTab[idxFillMat,] <- endTabOut
  
  return(endTab)
}