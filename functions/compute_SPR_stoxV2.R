compute_SPR_stoxV2 <- function( endTab,
                                ageVec,
                                surveyYear,
                                strataAll,
                                strata3a,
                                strata4){
  
  nAges   <- length(ageVec)
  minAge  <- min(ageVec)
  maxAge  <- max(ageVec)
  
  #############################################################################
  ## fill in table
  #############################################################################
  
  out         <- fill_missing_cells_SPR_stoxV2(endTab,mode='Abundance')
  endTab      <- out$endTab
  endTabInit  <- out$endTabInit
  
  #############################################################################
  ## create indices
  #############################################################################
  
  # initialize array nStrata x nAges x maturity stages
  indexHERAS.SPR <- array(0, 
                          dim=c(length(strataAll),nAges,3,3),
                          dimnames=list(ac(strataAll),
                                        ac(minAge:maxAge),
                                        c('all','MAT','IMM'),c('abu','biomass','length')))
  
  uniqueStrata <- unique(endTab$strata)
  
  for(idxStrata in uniqueStrata){
    endTabFilt.strata  <- endTab[endTab$strata == idxStrata,]
    
    # loop on all the index ages (1-9+) with 9 as plus group
    for(idxAges in ageVec){
      
      # select age to be computed, make sure we combine ages for the plus group
      if(idxAges == maxAge){
        # select ages as plut grounp
        ageSel <- unique(endTabFilt.strata$IndividualAge)[unique(endTabFilt.strata$IndividualAge) >= idxAges]
      }else{
        # select current age
        ageSel <- idxAges
      }
      
      # all stages
      endTabFilt  <- endTabFilt.strata[ endTabFilt.strata$age %in% ageSel,]
      
      indexHERAS.SPR[ac(idxStrata),ac(idxAges),'all','abu'] <- sum(endTabFilt$Abundance,na.rm=TRUE)
      indexHERAS.SPR[ac(idxStrata),ac(idxAges),'all','biomass'] <- sum(endTabFilt$biomass_g,na.rm=TRUE)
      indexHERAS.SPR[ac(idxStrata),ac(idxAges),'all','length'] <- sum(endTabFilt$length*endTabFilt$Abundance,na.rm=TRUE)
      
      # IMM stages
      endTabFilt.IMM  <- endTabFilt[ endTabFilt.strata$age %in% ageSel &
                                       endTabFilt.strata$maturity == 'IMM',]
      
      indexHERAS.SPR[ac(idxStrata),ac(idxAges),'IMM','abu'] <- sum(endTabFilt.IMM$Abundance,na.rm=TRUE)
      indexHERAS.SPR[ac(idxStrata),ac(idxAges),'IMM','biomass'] <- sum(endTabFilt.IMM$biomass_g,na.rm=TRUE)
      indexHERAS.SPR[ac(idxStrata),ac(idxAges),'IMM','length'] <- sum(endTabFilt.IMM$length*endTabFilt.IMM$Abundance,na.rm=TRUE)
      
      # MAT stages
      endTabFilt.MAT  <- endTabFilt[ endTabFilt.strata$age %in% ageSel &
                                       endTabFilt.strata$maturity == 'MAT',]
      
      indexHERAS.SPR[ac(idxStrata),ac(idxAges),'MAT','abu'] <- sum(endTabFilt.MAT$Abundance,na.rm=TRUE)
      indexHERAS.SPR[ac(idxStrata),ac(idxAges),'MAT','biomass'] <- sum(endTabFilt.MAT$biomass_g,na.rm=TRUE)
      indexHERAS.SPR[ac(idxStrata),ac(idxAges),'MAT','length'] <- sum(endTabFilt.MAT$length*endTabFilt.MAT$Abundance,na.rm=TRUE)
    }
  }
  
  indexHERAS.SPR3a  <- indexHERAS.SPR[strata3a,,,]
  indexHERAS.SPR4   <- indexHERAS.SPR[strata4,,,]
  
  ####################################
  # weight at age vector
  ####################################
  waMat4 <- as.data.frame(array(0, dim=c(nAges,4)))
  colnames(waMat4) <- c('age','MAT','IMM','all')
  waMat4$age <- ageVec
  
  waMat4[,'MAT'] <- apply(indexHERAS.SPR4[,,'MAT','biomass'],2,sum)/
    apply(indexHERAS.SPR4[,,'MAT','abu'],2,sum)
  waMat4[,'IMM'] <- apply(indexHERAS.SPR4[,,'IMM','biomass'],2,sum)/
    apply(indexHERAS.SPR4[,,'IMM','abu'],2,sum)
  waMat4[,'all'] <- apply(indexHERAS.SPR4[,,'all','biomass'],2,sum)/
    apply(indexHERAS.SPR4[,,'all','abu'],2,sum)
  
  waMat3a <- as.data.frame(array(0, dim=c(nAges,4)))
  colnames(waMat3a) <- c('age','MAT','IMM','all')
  waMat3a$age <- ageVec
  
  waMat3a[,'MAT'] <-  apply(indexHERAS.SPR3a[,,'MAT','biomass'],2,sum)/
    apply(indexHERAS.SPR3a[,,'MAT','abu'],2,sum)
  waMat3a[,'IMM'] <-  apply(indexHERAS.SPR3a[,,'IMM','biomass'],2,sum)/
    apply(indexHERAS.SPR3a[,,'IMM','abu'],2,sum)
  waMat3a[,'all'] <-  apply(indexHERAS.SPR3a[,,'all','biomass'],2,sum)/
    apply(indexHERAS.SPR3a[,,'all','abu'],2,sum)
  
  ####################################
  # length at age vector
  ####################################
  laMat4 <- as.data.frame(array(0, dim=c(nAges,4)))
  colnames(laMat4) <- c('age','MAT','IMM','all')
  laMat4$age <- ageVec
  
  laMat4[,'MAT'] <- apply(indexHERAS.SPR4[,,'MAT','length'],2,sum)/
    apply(indexHERAS.SPR4[,,'MAT','abu'],2,sum)
  laMat4[,'IMM'] <- apply(indexHERAS.SPR4[,,'IMM','length'],2,sum)/
    apply(indexHERAS.SPR4[,,'IMM','abu'],2,sum)
  laMat4[,'all'] <- apply(indexHERAS.SPR4[,,'all','length'],2,sum)/
    apply(indexHERAS.SPR4[,,'all','abu'],2,sum)
  
  laMat3a <- as.data.frame(array(0, dim=c(nAges,4)))
  colnames(laMat3a) <- c('age','MAT','IMM','all')
  laMat3a$age <- ageVec
  
  laMat3a[,'MAT'] <-  apply(indexHERAS.SPR3a[,,'MAT','length'],2,sum)/
    apply(indexHERAS.SPR3a[,,'MAT','abu'],2,sum)
  laMat3a[,'IMM'] <-  apply(indexHERAS.SPR3a[,,'IMM','length'],2,sum)/
    apply(indexHERAS.SPR3a[,,'IMM','abu'],2,sum)
  laMat3a[,'all'] <-  apply(indexHERAS.SPR3a[,,'all','length'],2,sum)/
    apply(indexHERAS.SPR3a[,,'all','abu'],2,sum)
  
  ####################################
  # proportion mature vector
  ####################################
  propMat4 <- as.data.frame(array(0, dim=c(nAges,2)))
  colnames(propMat4) <- c('age','all')
  propMat4$age <- ageVec
  
  propMat4[,'all'] <- apply(indexHERAS.SPR4[,,'MAT','abu'],2,sum)/
    apply(indexHERAS.SPR4[,,'all','abu'],2,sum)
  
  propMat3a <- as.data.frame(array(0, dim=c(nAges,2)))
  colnames(propMat3a) <- c('age','all')
  propMat3a$age <- ageVec
  
  propMat3a[,'all'] <- apply(indexHERAS.SPR3a[,,'MAT','abu'],2,sum)/
    apply(indexHERAS.SPR3a[,,'all','abu'],2,sum)
  
  return(list(SPR.all = indexHERAS.SPR,
              SPR.3a = indexHERAS.SPR3a,
              SPR.4 = indexHERAS.SPR4,
              propM4   = propMat4,
              wa4      = waMat4,
              la4      = laMat4,
              propM3a  = propMat3a,
              wa3a      = waMat3a,
              la3a      = laMat3a,
              endTab  = endTab,
              endTabInit = endTabInit))
  
}