fill_missing_cells_SPR_stoxV4 <- function(endTab,mode='Cells'){
  
  endTab$filling_weightLengthAge         <- 0
  #unique(endTab$StockCode)
  #unique(endTab$IndividualAge)
  #unique(endTab$IndividualRoundWeight)
  #unique(endTab$IndividualTotalLength)
  endTab$IndividualMaturity[endTab$IndividualMaturity == ''] <- NA
  endTab <- endTab[!is.na(endTab$Abundance),]
  endTab <- endTab[endTab$Abundance > 0,]
  #unique(endTab$IndividualMaturity)
  
  #############################################################################
  ## fill in table
  #############################################################################
  
  endTab$filling_weightLengthAge        <- 0
  endTab$IndividualMaturity[endTab$IndividualMaturity == ''] <- NA
  endTab <- endTab[!is.na(endTab$Abundance),]
  
  # weight/length relationship
  growth <- tryCatch({    growth <- nls(formula = as.numeric(IndividualRoundWeight) ~ a*as.numeric(IndividualTotalLength)^b,
                              data = endTab[!is.na(endTab$IndividualRoundWeight) & !is.na(endTab$IndividualTotalLength), ],
                              start = list(a = 0.01, b= 2.8))
                          growth <- list(a=coef(growth)[1],b=coef(growth)[2])
            },error=function(e){
              message('An Error Occurred')
              print(e)
              a <- 0.01
              b <- 2.8
              return(list(a=0.01,b=2.8))})

  # length/age relationship
  growthAge <- tryCatch({  growthAge <- nls(formula = as.numeric(IndividualTotalLength) ~ Linf * (1 - exp(-K*(as.numeric(IndividualAge) - t0))),
                               data = endTab[!is.na(endTab$IndividualAge) & !is.na(endTab$IndividualTotalLength), ],
                               start = list(K = 0.9, Linf= 13,t0=-0.4))
                          growthAge <- list(K = coef(growthAge)[1],
                                    Linf= coef(growthAge)[2],
                                    t0=-coef(growthAge)[3])},
           error=function(e){
                message('An Error Occurred')
                print(e)
                return(list(K=0.9,Linf=13,t0=-0.4))})

  
  unique(endTab[!is.na(endTab$IndividualAge) | !is.na(endTab$IndividualTotalLength), ]$IndividualTotalLength)

  idxFillMat <- which(is.na(endTab$IndividualMaturity) | 
                        is.na(endTab$IndividualAge))
  
  endTabOut <- endTab[idxFillMat,]
  
  # fill in age 0 to immature
  idxEmptyMat     <-  which(is.na(endTabOut$IndividualMaturity))
  idxTemp         <- idxEmptyMat[endTabOut$IndividualAge[idxEmptyMat] == 0 & !is.na(endTabOut$IndividualAge[idxEmptyMat])]
  if(length(idxTemp != 0)){
    print(paste0('fill in age 0 to immature - N=',length(idxTemp)))
    endTabOut[idxTemp,]$IndividualMaturity  <- 'IMM'
    endTabOut[idxTemp,]$filling_weightLengthAge       <- 1
  }
  
  # fill in fish of length < 7 cm as IMM
  idxEmptyMat   <-  which(is.na(endTabOut$IndividualMaturity))
  idxTemp       <- idxEmptyMat[endTabOut$IndividualTotalLength[idxEmptyMat] < 7]
  if(length(idxTemp != 0)){
    print(paste0('fill in fish of length < 7 cm as IMM - N=',length(idxTemp)))
    endTabOut[idxTemp,]$IndividualMaturity <- 'IMM'
    endTabOut[idxTemp,]$filling_weightLengthAge       <- 1
  }
  
  # fill in missing lengths
  idxEmptyLength  <-  which(is.na(endTabOut$IndividualTotalLength))
  if(length(idxEmptyLength) != 0){
    print(paste0('fill in missing lengths - N=',length(idxEmptyLength)))
    endTabOut$IndividualTotalLength[idxEmptyLength] <- (endTabOut$IndividualRoundWeight[idxEmptyLength]/growth$a)^(1-growth$b)
    endTabOut$filling_weightLengthAge[idxEmptyLength] <- 1
  }

  # fill in missing age
  idxEmptyAge  <-  which(is.na(endTabOut$IndividualAge))
  if(length(idxEmptyAge) != 0){
    print(paste0('fill in missing lengths - N=',length(idxEmptyAge)))
    L <- endTabOut$IndividualTotalLength[idxEmptyAge]
    missingAge <- rep(NA, length(idxEmptyAge))
    missingAge[L>=growthAge$Linf]   <- max(endTab$IndividualAge,na.rm=TRUE)
    missingAge[L<growthAge$Linf]  <- -1/growthAge$K*log(1-L[L<growthAge$Linf]/growthAge$Linf)+growthAge$t0
    missingAge[L <= 0] <- 0
    endTabOut$IndividualAge[idxEmptyAge] <- missingAge
    endTabOut$filling_weightLengthAge[idxEmptyLength] <- 1
  }
  
  
  # fill in missing weights
  idxEmptyWeight  <-  which(is.na(endTabOut$IndividualRoundWeight))
  if(length(idxEmptyWeight) != 0){
    print(paste0('fill in missing weights - N=',length(idxEmptyWeight)))
    yPred <- growth$a*endTabOut$IndividualTotalLength[idxEmptyWeight]^growth$b
    endTabOut$IndividualRoundWeight[idxEmptyWeight] <- yPred
    endTabOut$filling_weightLengthAge[idxEmptyWeight] <- 1
  }
  
  # fill in ages 0 for fish < 5 cm
  idxEmptyAge     <-  which(is.na(endTabOut$IndividualAge))
  idxTemp       <- idxEmptyMat[endTabOut$IndividualTotalLength[idxEmptyAge] < 6.5]
  if(length(idxTemp != 0)){
    print(paste0('fill in length < 5cm to age 0 - N=',length(idxTemp)))
    endTabOut[idxTemp,]$IndividualAge  <- 0
    endTabOut[idxTemp,]$filling_weightLengthAge       <- 1
  }
  
  # fill in fish of age > 4 as MAT
  idxEmptyMat   <-  which(is.na(endTabOut$IndividualMaturity))
  idxTemp       <- idxEmptyMat[endTabOut$IndividualAge[idxEmptyMat] > 4]
  if(length(idxTemp != 0)){
    print(paste0('fill in fish of age > 4 as MAT - N=',length(idxTemp)))
    endTabOut[idxTemp,]$IndividualMaturity <- 'MAT'
    endTabOut[idxTemp,]$filling_weightLengthAge       <- 1
  }
  
  # replace values in tab
  endTab[idxFillMat,] <- endTabOut
  endTabInit <- endTab
  
  ###########################################
  # aggregate table
  ###########################################
  
  # calculate biomass for each entry
  #endTab$biomass<-as.numeric(as.character(endTab$Abundance))*as.numeric(as.character(endTab$weight))
  endTab <- endTab[!is.na(endTab$Abundance),]
  endTab <- endTab[endTab$Abundance > 0,]
  endTab <- endTab %>%
            group_by(Stratum,IndividualTotalLength,IndividualAge,IndividualRoundWeight,IndividualMaturity,filling_weightLengthAge) %>%
            summarize(Abundance=sum(Abundance),
                      Biomass=sum(Biomass))
  
  names(endTab)<-c("strata","length","age","weight","maturity","filling_weightLengthAge","Abundance","Biomass")
  
  endTab$Biomass[is.na(endTab$Biomass)] <- endTab$Abundance[is.na(endTab$Biomass)]*endTab$weight[is.na(endTab$Biomass)]
  
  endTabInit <- endTab
  
  ###########################################
  # fill in maturity and stock id
  ###########################################
  endTab$filling_mat_ID <- 0
  
  idxFillMat <- which(  is.na(endTab$maturity)| 
                          is.na(endTab$age)| 
                          is.na(endTab$length))
  
  endTabOut <- endTab[idxFillMat,]
  if(length(idxFillMat)!=0){
    endTabOut$filling_mat_ID <- 1
    
    for(idxSpecimen in 1:dim(endTabOut)[1]){
      length_inter  <- 0
      probAll       <- array(-1,dim=c(2,1))
      probAllIMM    <- -1
      flag_age      <- FALSE
      
      # if there is an age
      if(any(endTab$strata == endTabOut$strata[idxSpecimen]&
             endTab$age == endTabOut$age[idxSpecimen]&
             !is.na(endTab$maturity))){
        endTabFiltAge <- endTab[0,0]
        while((probAllIMM == -1 | probAllIMM == 0.5) & 
              dim(endTab[  endTab$strata == endTabOut$strata[idxSpecimen]&
                           endTab$age == endTabOut$age[idxSpecimen]&
                           !is.na(endTab$maturity),])[1] > dim(endTabFiltAge)[1]){
          endTabFiltAge <- endTab[  endTab$strata == endTabOut$strata[idxSpecimen] &
                                      endTab$age == endTabOut$age[idxSpecimen] &
                                      endTab$length >= (endTabOut$length[idxSpecimen]-length_inter) &
                                      endTab$length <= (endTabOut$length[idxSpecimen]+length_inter) &
                                      !is.na(endTab$maturity),]
          length_inter <- length_inter + 1
          
          if(dim(endTabFiltAge)[1] != 0){
            if(mode == 'Cells'){
              probAllIMM  <- dim(endTabFiltAge[endTabFiltAge$maturity == 'IMM',])[1]/dim(endTabFiltAge)[1]
            }else if(mode == 'Abundance'){
              probAllIMM  <- sum(endTabFiltAge[endTabFiltAge$maturity == 'IMM',]$Abundance)/sum(endTabFiltAge$Abundance)
            }
          }
        }
        if(probAllIMM != 0.5)flag_age <- TRUE
      }
      
      # if no age, go with length
      if(flag_age == FALSE){
        endTabFiltLength <- endTab[0,0]
        while((probAllIMM == -1 | probAllIMM == 0.5)&
              dim(endTab[  endTab$strata == endTabOut$strata[idxSpecimen]&
                           !is.na(endTab$maturity),])[1] > dim(endTabFiltLength)[1]){
          endTabFiltLength <- endTab[ endTab$strata == endTabOut$strata[idxSpecimen] &
                                        endTab$length >= (endTabOut$length[idxSpecimen]-length_inter) &
                                        endTab$length <= (endTabOut$length[idxSpecimen]+length_inter) &
                                        !is.na(endTab$maturity),]
          if(dim(endTabFiltLength)[1] != 0){
            if(mode == 'Cells'){
              probAllIMM  <- dim(endTabFiltLength[endTabFiltLength$maturity == 'IMM',])[1]/dim(endTabFiltLength)[1]
            }else if(mode == 'Abundance'){
              probAllIMM  <- sum(endTabFiltLength[endTabFiltLength$maturity == 'IMM',]$Abundance)/sum(endTabFiltLength$Abundance)
            }
            length_inter <- length_inter + 1
          }else length_inter <- length_inter + 1
        }
      }
      
      # allocate maturity field
      if(probAllIMM >= 0.5)endTabOut[idxSpecimen,]$maturity <- 'IMM'else endTabOut[idxSpecimen,]$maturity <- 'MAT'
    }
  }
  
  endTab[idxFillMat,] <- endTabOut
  
  out <- list(endTabInit = endTabInit,
              endTab = endTab)
  
  return(out)
}
